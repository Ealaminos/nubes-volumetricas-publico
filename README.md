# Nubes Volumétricas

Este repositorio contiene el código fuente del Trabajo de Fin de Grado realizado por Luna Alaminos. En este trabajo se estudia una de las técnicas para la simulación de nubes, se basó en la siguiente tesis: *Fredrik Häggström. Real-time rendering of volumetric clouds, 2018.* La tecnica estudiada utiliza varias texturas 2D y 3D generadas a través de ruido procedural (Perlin y Worley). Para la visualización de las nubes se utiliza el algoritmo de *ray marching*.

## Características

La aplicación que se ha implementado permite la visualización de nubes volumétricas, además incluye una interfaz donde se puede:

- Cambiar los parámetros del cielo como la posición del sol.
- Cambiar el tamaño y densidad de las nubes.
- Cambiar la iluminación de las nubes.
- Animar las nubes en una dirección y velocidad.

Entre otras características, podéis consultar la wiki para más información sobre la interfaz: [link](https://gitlab.com/Ealaminos/nubes-volumetricas-publico/-/wikis/home/interfaz).

## FastNoise

*FastNoise* es una librería de código abierto de generación de ruido procedural con una gran colección de algoritmos de ruido. Se ha utilizado una modificación que permite generar ruido periódico: [link](https://github.com/Auburns/FastNoise/tree/8e015c33d3d5478dadff5057c00ed058823f3a7f). Se han realizado varios cambios para que permita utilizar el movimiento Browniano Fraccional (*Fractional Brownian motion*) en la modificación. Podéis encontrar el código en este mismo repositorio en *FastNoise/*, además de un ejemplo para generar las texturas que se utilizan en la aplicación principal.

## Demo

<iframe src="https://itch.io/embed-upload/2333922?color=333333" allowfullscreen="" width="1280" height="740" frameborder="0"><a href="https://alduen.itch.io/nubes-volumetricas">Prueba la aplicación en itch.io</a></iframe>

## Ejemplo de la animación

[![Nubes volumétricas en Youtube](http://img.youtube.com/vi/A5XeOjqtLjk/0.jpg)](http://www.youtube.com/watch?v=A5XeOjqtLjk "Ejemplo de las nubes volumétricas")
